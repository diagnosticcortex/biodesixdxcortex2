%  This script tests the execution of the Prostate development and
%  validation workflows for the dxcortex random forest comparison.
%
clear;
format long;
warning('off','all'); % We get warnings in the logistic regression
                      % this is handled.

% STEP 1 - Load Feature Table for development data
csvfile = '.\ExampleData\Lymphoma\DevFT_RndReal_6.csv';
devData = readtable(csvfile);
% Filename | Groupname | Definition | Ftrs ...
filenames = table2array(devData(2:end,1));
groupnames = table2array(devData(2:end,2));
definitions = double(strcmp(table2array(devData(2:end,3)),LymphomaWorkflow.group2));
osdata = str2double(table2array(devData(2:end,5)));
oscensor = str2double(table2array(devData(2:end,6)));
ftrTable = table2array(devData(2:end,7:end));
ftrNames = table2array(devData(1,7:end));

% STEP 2 - Load Feature Table for validation data
csvfile = '.\ExampleData\Lymphoma\IntVal_FT.csv';
valData = readtable(csvfile);
% Filename | Groupname | Definition | Ftrs ...
valFilenames = table2array(valData(2:end,1));
valGroupnames = table2array(valData(2:end,2));
valDefinitions = double(strcmp(table2array(valData(2:end,3)),LymphomaWorkflow.group2));
valFtrTable = table2array(valData(2:end,7:end));
valFtrNames = table2array(valData(1,7:end)); % Better be the same as development!

% STEP 3 - Set up output files
outputDev = '.\Output\Lymphoma\ComparisonDev.xlsx';
outputVal = '.\Output\Lymphoma\ComparisonVal.xlsx';

% STEP 4 - Instantiate and call the prostate workflow
lymphomaWorkflow = LymphomaWorkflow;
comparisonDev = lymphomaWorkflow.RunDevelopmentWorkflow(filenames, groupnames, definitions, osdata, oscensor, ftrTable, ftrNames, outputDev);
comparisonVal = lymphomaWorkflow.RunValidationWorkflow(valFilenames, valGroupnames, valDefinitions, valFtrTable, outputVal);

