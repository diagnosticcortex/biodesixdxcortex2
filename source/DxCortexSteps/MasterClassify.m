% Applies the logistic regression model to a test matrix of 0's and 1's 
% to obtain the final classification probabilities and labels.
function [probs, labels] = MasterClassify(miniClassifications, logRegCoefficients, probCutoff)
    exponents = logRegCoefficients(1) + miniClassifications * logRegCoefficients(2:end);
    probs = 1.0 ./ (1.0 + exp(-1.0 .* exponents));
    labelsLogical = probs > probCutoff;
    labels = double(labelsLogical);
end