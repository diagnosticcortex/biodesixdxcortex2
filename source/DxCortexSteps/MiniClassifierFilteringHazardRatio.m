% Performs mini-classifier (KNN) filtering given Hazard Ratio criteria.
function [passingMiniClassifiers, trainingFtrTable, trainingDefinitions] = MiniClassifierFilteringHazardRatio(filenames, groupnames, ... 
    numericDefinitions, osdata, oscensor, ftrTable, ftrNames, useAllFeaturesAllLevels, ...
    maxFeaturesToUse, k, trainingGroup1, trainingGroup2, minHR, ... 
    maxHR, rowLogicalForFilter)

   numFtrs = length(ftrNames);
      
   % To be consistent with Nextgen, always put training group 2 rows first
   % in the trainingFtrTable, then training group 1
   trainingGroup2Rows = strcmp(groupnames, trainingGroup2);
   trainingGroup1Rows = strcmp(groupnames, trainingGroup1);
   
   trainingFtrTable = [ftrTable(trainingGroup2Rows,:); ftrTable(trainingGroup1Rows,:)];     
   Y = [numericDefinitions(trainingGroup2Rows); numericDefinitions(trainingGroup1Rows)];
   trainingDefinitions = Y;
   trainingOS = osdata(rowLogicalForFilter);
   trainingOSCensor = oscensor(rowLogicalForFilter);
   
   count = 0;
   passingMiniClassifiers = {};
   passingLevel1 = [];
   for i=1:maxFeaturesToUse 
       % Try all combinations (without repeating) of size i from features
       combos = nchoosek(1:numFtrs, i);
       if useAllFeaturesAllLevels
           for j=1:length(combos)
               X = trainingFtrTable(:,combos(j,:));
               XTest = ftrTable(rowLogicalForFilter,combos(j,:));
               knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
               [labels,~,~] = predict(knnClass,XTest);
               
               % HR Computation
               b = coxphfit(labels,trainingOS,'Censoring',~trainingOSCensor);
               hr = exp(-b);
               if hr >= minHR && hr <= maxHR
                   count = count + 1;
                   passingMiniClass.ftrs = combos(j,:);
                   passingMiniClass.kNNClass = knnClass;                   
                   passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
               end
                   
           end
       else          
           if i == 1
               for j=1:length(combos)
                   X = trainingFtrTable(:,combos(j,:));
                   XTest = ftrTable(rowLogicalForAccuracy,combos(j,:));
                   YTest = numericDefinitions(rowLogicalForAccuracy);
                   knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                   [labels,~,~] = predict(knnClass,XTest);
                  % HR Computation
                   b = coxphfit(trainingOS,labels,'Censoring',trainingOSCensor);
                   hr = exp(b);
                   if hr >= minHR && hr <= maxHR
                       count = count + 1;
                       passingLevel1 = [passingLevel1;combos(j,:)];
                       passingMiniClass.ftrs = combos(j,:);
                       passingMiniClass.kNNClass = knnClass;                   
                       passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
                   end
               end
           else
               combos = nchoosek(passingLevel1, i);
               for j=1:length(combos)
                   X = trainingFtrTable(:,combos(j,:));
                   XTest = ftrTable(rowLogicalForAccuracy,combos(j,:));
                   YTest = numericDefinitions(rowLogicalForAccuracy);
                   knnClass = fitcknn(X,Y,'NumNeighbors',k,'NSMethod','exhaustive','distance','euclidean');
                   [labels,~,~] = predict(knnClass,XTest);
                   % HR Computation
                   b = coxphfit(trainingOS,labels,'Censoring',trainingOSCensor);
                   hr = exp(b);
                   if hr >= minHR && hr <= maxHR
                       count = count + 1;
                       passingMiniClass.ftrs = combos(j,:);
                       passingMiniClass.kNNClass = knnClass;                   
                       passingMiniClassifiers = [passingMiniClassifiers passingMiniClass];
                   end
               end
           end
       end         
   end
end