classdef LymphomaWorkflow < handle
    % This class implements the development and validation parts of the
    % prostate random forest comparison analysis workflow for the DxCortex.
    
    properties(Constant)
        group1 = 'Group1'; %0
        group2 = 'Group2';  %1
    end
    
    properties(Access=private)
        miniClassifiers = {};  % Holds the mini classifiers that pass 
                               % filtering in the development workflow.
        mcLogRegCoefficients = []; % Holds the logistic regression coefficients 
                                   % for the mini classifiers that pass filtering 
                                   % in the development workflow.
        trainingFtrTable = []; % Holds the training set feature table for ranking
                               % test instances in the RankKNN
        trainingDefs = []; % Holds the numeric definitions from the training 
                           % set feature table.
    end
    
    methods 
        % This function runs the prostate random forest development 
        % workflow on the provided training set data and writes the output 
        % Comparisons file to the specified output path/filename.
        %
        % The input training set data are:
        % 1) filenames - A cell array of the filenames or sampleIDs
        %                corresponding to the rows of the feature table.
        % 2) groupnames - A cell array of the groupnames (same row order as
        %                 filenames)
        % 3) definitions - A numeric array of 1's and 0's corresponding
        %                  to the alive(0) or dead(1) definition. These must 
        %                  be in the same order as filenames.
        % 4) osdata - Overall Survival data.
        % 5) oscensor - Overall Survivla censor value.
        % 6) ftrTable - A 2D numeric array containing the training set
        %               feature table (rows are filename, columns are
        %               features)
        % 7) ftrNames - The names of the features in ftrTable, i.e. column
        %               headers.
        %
        % The comparisonCellArray is returned as well as written to the
        % specified output path/filename.  The comparisonCellArray contains 
        % the following columns:
        % 1) Filename - The filename or sampleID for each row in the
        %               training set feature table.
        % 2) Groupname - The associated group name.
        % 3) Definition - The associated definition label.
        % 4) Probability - The numeric probability from applying the
        %                  developed master classifier to the training row. 
        % 5) Classification - The classification label from the master
        %                     classifier.
        % 6) ClassificationMatch - A 'y' or 'n' indicating a match (or not)
        %                          of the classification label and the 
        %                          definition for a sample.
        %
        function comparisonCellArray = RunDevelopmentWorkflow(self, filenames, groupnames, definitions, osdata, oscensor, ftrTable, ftrNames, outputFullyJustifiedFilename)
            % STEP 1 - Mini Classifier Filtering - RankKNN, k = 7, use only 
            % single features that pass, use up to pairs
            useAllFeaturesAllLevels = true;
            maxFeaturesToUse = 1; 
            k = 9;
            minHR = 1.3;
            maxHR = 100;
            
            % Get the training feature table for RankKNN
            %trainingRows = strcmp(groupnames,self.group1) | (strcmp(groupnames, self.group2));
            %self.trainingFtrTable = ftrTable(trainingRows,:);   
            rowLogicalForFilter = strcmp(groupnames,self.group1) | (strcmp(groupnames, self.group2));
            
            % Use all the training data for the accuracy filter 
            [self.miniClassifiers, self.trainingFtrTable, self.trainingDefs] = MiniClassifierFilteringHazardRatio(filenames, groupnames, ...
                definitions, osdata, oscensor, ftrTable, ftrNames, useAllFeaturesAllLevels, ...
                maxFeaturesToUse, k, self.group1, self.group2, minHR, maxHR, rowLogicalForFilter);
            
            % STEP 2) Logistic Regression w/ Dropout
            trainingMiniClassifications = ComputeKNNMiniClassifications(self.trainingFtrTable, self.miniClassifiers);
            self.mcLogRegCoefficients = LogisticRegressionWithDropout(trainingMiniClassifications, ...
                self.trainingDefs, 100000, 10);
            
            % STEP 3) Apply to all data in the feature table to get probabilities and labels           
            probCutoff = 0.5;
            allMiniClassifications = ComputeKNNMiniClassifications(ftrTable, self.miniClassifiers);
            [probs, labels] = MasterClassify(allMiniClassifications, self.mcLogRegCoefficients, probCutoff);
            classLabels = cell(length(labels),1);
            classLabels(labels==0) = {self.group1};
            classLabels(not(labels==0)) = {self.group2};
            
            % STEP 4) Comparison Step and Assemble Results
            comparisons = (labels==definitions);
            comparisonsYN = cell(length(comparisons),1);
            comparisonsYN(comparisons) = {'y'};
            comparisonsYN(not(comparisons)) = {'n'};
            
            % STEP 5) Write Output
            comparisonCellArray = {'Filename','Groupname','Definition','Probability','Classification','ClassificationMatch'};
            comparisonCellArray = [comparisonCellArray; filenames groupnames num2cell(definitions) num2cell(probs) classLabels comparisonsYN];
            writeCellArrayAsCSV(comparisonCellArray,outputFullyJustifiedFilename);
        end
        
        % This function runs the prostate random forest comparison validation 
        % workflow on the provided test set feature table and writes the output 
        % comparisons .xlsx file to the specified output path/filename.
        %
        % The input training set data are:
        % 1) filenames - A cell array of the filenames or sampleIDs
        %                corresponding to the rows of the feature table.
        % 2) groupnames - A cell array of the groupnames (same row order as
        %                 filenames)
        % 3) definitions - A numeric array of 1's and 0's corresponding
        %                  to the alive(0) or dead(1) definition. These must 
        %                  be in the same order as filenames.
        % 4) ftrTable - A 2D numeric array containing the training set
        %               feature table (rows are filename, columns are
        %               features)  Must contain the same features in the
        %               same order as the training feature table in the
        %               development workflow.
        %
        % The comparisonCellArray is returned as well as written to the
        % specified output directory and filename.  The comparisonCellArray 
        % contains the following columns:
        % 1) Filename - The filename or sampleID for each row in the
        %               training set feature table.
        % 2) Groupname - The associated group name.
        % 3) Definition - The associated definition label.
        % 4) Probability - The numeric probability from applying the
        %                  developed master classifier to the training row. 
        % 5) Classification - The classification label from the master
        %                     classifier.
        % 6) ClassificationMatch - A 'y' or 'n' indicating a match (or not)
        %                          of the classification label and the 
        %                          definition for a sample.
        %
        function comparisonCellArray = RunValidationWorkflow(self, filenames, groupnames, definitions, ftrTable, outputFullyJustifiedFilename)
            if (isempty(self.miniClassifiers))
                error('Prostate Workflow Error (no mini-classifiers) -- The user must run the development workflow before running the Validation workflow.')
            end
            
            % STEP 1) Apply to all data in the feature table to get probabilities and labels
            probCutoff = 0.5;
            allMiniClassifications = ComputeKNNMiniClassifications(ftrTable, self.miniClassifiers);
            [probs, labels] = MasterClassify(allMiniClassifications, self.mcLogRegCoefficients, probCutoff);
            classLabels = cell(length(labels),1);
            classLabels(labels==0) = {self.group1};
            classLabels(not(labels==0)) = {self.group2};
            
            % STEP 2) Comparison Step and Assemble Results
            comparisons = (labels==definitions);
            comparisonsYN = cell(length(comparisons),1);
            comparisonsYN(comparisons) = {'y'};
            comparisonsYN(not(comparisons)) = {'n'};
            
            % STEP 3) Write Output
            comparisonCellArray = {'Filename','Groupname','Definition','Probability','Classification','ClassificationMatch'};
            comparisonCellArray = [comparisonCellArray; filenames groupnames num2cell(definitions) num2cell(probs) classLabels comparisonsYN];
            writeCellArrayAsCSV(comparisonCellArray,outputFullyJustifiedFilename)       
        end        
    end
end