%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Description:                                                            
%-------------------------------------------------------------------------
% This function saves into a .csv file, a cell array which elements don't 
% need to be all of the same type
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Inputs:
%-------------------------------------------------------------------------
%CELLARRAY - cell array to save
%OUTFILENAME - path to the .csv file where the array will be saved
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Outputs:                                                                
%-------------------------------------------------------------------------
%No outputs
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Intended Use:
%-------------------------------------------------------------------------
%This function is handy when needing to create .csv files from cell arrays
%with different kinds of data (doubles, string, ...) since the csvwrite
%only works for matrixes of doubles.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Dependencies:
%-------------------------------------------------------------------------
%No dependencies

function [] = writeCellArrayAsCSV(cellArray,outFileName)
    T = cell2table(cellArray);
    writetable(T,outFileName,'WriteVariableNames',0);
end
