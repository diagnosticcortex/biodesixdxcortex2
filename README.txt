Biodesix Diagnostic Cortex (DxCortex) 2

Contact Information - DxCortex@biodesix.com

NOTE:
The DxCortex itself is implemented in C# and uses HPC to facilitate running large numbers of experiments with large data sets.
This repository contains Matlab code that demonstrates the execution of the DxCortex in the context of the experiments shown in 
"Robust Identification of Molecular Phenotypes using Semi-Supervised Learning".  The full data set to reproduce the results of the 
experiments shown in the paper is very large, so one instance of example data is provided along with the original full data sets.  
If more data or information on how the random data was created is desired, please contact the authors at DxCortex@biodesix.com.  

OPERATIONAL EXECUTION:
This source code is written in Matlab and requires Matlab R2017a to run. An example is given for the Lymphoma workflow with intermediate 
filtering used in the paper.  To run this, start Matlab, add the install location of the local copy of the repository and all subdirectories 
to the Matlab path, and change the current directory to be BiodesixDxCortex2 in Matlab.  This example runs a classifier development 
procedure for the given development data set, and then classifies that data set and a validation data set.  Be sure to delete any 
existing output data before executing the test script.

EXAMPLE 1: Lymphoma
Run TestLymphomaWorkflow.m - Classifier Development is performed on one realization of the lymphoma data set,
then Classification is performed using the developed Dropout Regularized Combination Classifier on the development data set and on 
a validation data set. Output files are produced including output probabilities, output classification labels, and a 'y' or 'n' indicating
if the classification label matched (or did not match) the assigned definition for each sample.  